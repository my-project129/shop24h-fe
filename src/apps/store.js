import { createStore, combineReducers } from "redux";
import OrderCart from "../components/Redux Store/OrderCart";

const appReducer = combineReducers({
  taskReducer: OrderCart,
});

const store = createStore(
  appReducer,
  {},
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
