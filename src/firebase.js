import firebase from "firebase/app";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCs0__sV61LsylteMq-A-wEpoR5HROHVdw",
    authDomain: "devcampr12.firebaseapp.com",
    projectId: "devcampr12",
    storageBucket: "devcampr12.appspot.com",
    messagingSenderId: "462373674905",
    appId: "1:462373674905:web:6b3f1c1d0dea75e9303d08"
  };

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();