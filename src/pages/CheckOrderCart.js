import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import CheckOrderCartContent from "../components/Content/CheckOrderCartContent/CheckOrderCartContent";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import { Row, Col } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";

export default function CheckOrderCart({ user, logoutGoogle }) {
  const breadcrumbData = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Products",
      url: "/products",
    },
    {
      name: "Your cart",
      url: "/checkOrderCart",
    },
  ];

  var retrievedData = sessionStorage.getItem("orderArray");
  var retrievedArray = JSON.parse(retrievedData);
  //console.log(retrievedArray);

  const navigate = useNavigate();
  const onBtnGoBackClickHandler = () => {
    navigate("/products");
  };

  return (
    <>
      <div className="container">
        <Header user={user} logoutGoogle={logoutGoogle} />
        <div style={{ marginTop: "100px" }}>
          <BreadCrumb breadcrumbData={breadcrumbData} />
          {retrievedData ? (
            <CheckOrderCartContent user={user} />
          ) : (
            <h1>Bạn chưa chọn mua mặt hàng nào</h1>
          )}
          {/* <CheckOrderCartContent /> */}
        </div>
        <Row>
          <Col className="mt-3 text-center">
            <button
              className="btn btn-primary m-2"
              onClick={onBtnGoBackClickHandler}
            >
              Go Back
            </button>
          </Col>
        </Row>
      </div>
      <Footer />
    </>
  );
}
