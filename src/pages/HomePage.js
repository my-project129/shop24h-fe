import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import HomePageContent from "../components/Content/HomePage/HomePageContent/HomePageContent";

export default function HomePage({ user, logoutGoogle }) {
  return (
    <>
      <div className="container">
        <Header user={user} logoutGoogle={logoutGoogle} />
        <HomePageContent />
      </div>
      <Footer />
    </>
  );
}
