import { auth, googleProvider } from "../firebase";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

export default function Login({ loginGoogle }) {
  const [user, setUser] = useState(null);
  // const [objectRequest, setObjectRequest] = useState({});
  const { param } = useParams();
  const navigate = useNavigate();

  const getData = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };

  const onBtnSignInWithGoogleClick = () => {
    loginGoogle();
    navigate("/");
  };

  const logoutGoogle = () => {
    auth
      .signOut()
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        //console.log(error);
      });
  };

  let resultPhone = "";
  const getPhoneNumber = (paramPhone) => {
    if (paramPhone === null || paramPhone === undefined) {
      resultPhone = "123465789";
    } else {
      resultPhone = paramPhone;
    }
  };

  useEffect(() => {
    auth.onAuthStateChanged((result) => {
      // //console.log(result);
      setUser(result);
      getData(`https://shop24h-backend-namvu.herokuapp.com/customers/`)
        .then((data) => {
          //console.log(data.data);
          // Kiểm tra xem có sẵn user trong mongodb chưa, nếu chưa thì post thêm customer
          // if (data.data.includes(result.email) === false) {
          getPhoneNumber(result.phoneNumber);
          getData("https://shop24h-backend-namvu.herokuapp.com/customers", {
            method: "POST",
            body: JSON.stringify({
              fullName: result.displayName,
              phone: resultPhone,
              email: result.email,
            }),
            headers: {
              "Content-Type": "application/json;charset=UTF-8",
            },
          })
            .then((data2) => {
              //console.log(data2);
            })
            .catch((error) => {
              //console.log(error);
            });
          // }
        })
        .catch((error) => {
          //console.log(error);
        });
    });
  }, []);
  return (
    <div>
      <div>
        {/* <div className="d-flex justify-content-center align-items-center vh-100">
                    <div style={{ height: "500px" }}>
                        <div className="row align-self-center">
                            <button onClick={onBtnSignInWithGoogleClick} className="btn btn-danger">Sign in with google</button>
                        </div>
                        <div className="row">
                            or
                        </div>
                        <div className="row">
                            <input type="text" placeholder="Username"></input>
                        </div>
                        <div className="row">
                            <input type="password" placeholder="Password"></input>
                        </div>
                        <div className="row align-self-center">
                            <button className="btn btn-success">Sign in</button>
                        </div>
                        <div className="row align-self-center">
                            <span>Don't have an account? <a href="#">Sign up here</a></span>
                        </div>
                    </div>
                </div> */}
        <body
          style={{
            fontFamily: "'Poppins', sans-serif",
            backgroundColor: "#f0f2f5",
            color: "#1c1e21",
          }}
        >
          <main
            style={{
              height: "100vh",
              width: "100vw",
              position: "relative",
              margin: "0 auto",
            }}
          >
            <div
              class="row"
              style={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
                width: "100%",
                maxWidth: "1000px",
                position: "absolute",
                left: "50%",
                top: "50%",
                transform: "translate(-50%, -50%)",
              }}
            >
              <div
                class="colm-form"
                style={{
                  flex: "0 0 40%",
                  textAlign: "center",
                }}
              >
                <div
                  class="form-container"
                  style={{
                    backgroundColor: "#ffffff",
                    border: "none",
                    borderRadius: "10px",
                    boxShadow:
                      "0 2px 4px rgba(0, 0, 0, 0.1), 0 8px 16px rgba(0, 0, 0, 0.1)",
                    marginBottom: "30px",
                    padding: "20px",
                    maxWidth: "400px",
                  }}
                >
                  <button
                    onClick={onBtnSignInWithGoogleClick}
                    class="btn-login"
                    style={{
                      backgroundColor: "red",
                      border: "none",
                      borderRadius: "6px",
                      fontSize: "20px",
                      padding: "0 16px",
                      color: "#ffffff",
                      fontWeight: "700",
                      width: "100%",
                      margin: "5px 0",
                      height: "45px",
                      verticalAlign: "middle",
                      fontSize: "16px",
                    }}
                  >
                    Login with Google
                  </button>
                  <input
                    style={{
                      border: "1px solid #dddfe2",
                      color: "#1d2129",
                      padding: "0 8px",
                      outline: "none",
                      width: "100%",
                      margin: "5px 0",
                      height: "45px",
                      verticalAlign: "middle",
                      fontSize: "16px",
                    }}
                    type="text"
                    placeholder="Email address or phone number"
                  />
                  <input
                    type="password"
                    placeholder="Password"
                    style={{
                      border: "1px solid #dddfe2",
                      color: "#1d2129",
                      padding: "0 8px",
                      outline: "none",
                      width: "100%",
                      margin: "5px 0",
                      height: "45px",
                      verticalAlign: "middle",
                      fontSize: "16px",
                    }}
                  />
                  <button
                    class="btn-login"
                    style={{
                      backgroundColor: "#1877f2",
                      border: "none",
                      borderRadius: "6px",
                      fontSize: "20px",
                      padding: "0 16px",
                      color: "#ffffff",
                      fontWeight: "700",
                      width: "100%",
                      margin: "5px 0",
                      height: "45px",
                      verticalAlign: "middle",
                      fontSize: "16px",
                    }}
                  >
                    Login
                  </button>
                  <a
                    href="#"
                    style={{
                      display: "block",
                      color: "#1877f2",
                      fontSize: "14px",
                      textDecoration: "none",
                      padding: "10px 0 20px",
                      borderBottom: "1px solid #dadde1",
                    }}
                  >
                    Forgotten password?
                  </a>
                  <button
                    class="btn-new"
                    style={{
                      backgroundColor: "#42b72a",
                      border: "none",
                      borderRadius: "6px",
                      fontSize: "17px",
                      lineHeight: "48px",
                      padding: "0 16px",
                      color: "#ffffff",
                      fontWeight: "700",
                      marginTop: "20px",
                    }}
                  >
                    Create new Account
                  </button>
                </div>
              </div>
            </div>
          </main>
        </body>
      </div>
    </div>
  );
}
