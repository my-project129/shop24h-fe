import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import ProductDetailInfo from "../components/Product Detail/ProductDetailInfo";
import ProductDetailDescription from "../components/Product Detail/ProductDetailDescription";
import ViewAll from "../components/Content/HomePage/HomePageContent/ViewAll";
import ProductDetailRelated from "../components/Product Detail/ProductDetailRelated";

import BreadCrumb from "../components/BreadCrumb/BreadCrumb";

import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";

export default function ProductInfo({ user, logoutGoogle }) {
  let params = useParams();
  ////console.log(params);

  const breadcrumbData = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Products",
      url: "/products",
    },
    {
      name: "Detail",
      url: "/products/" + params.productId,
    },
  ];
  const [product, setProduct] = useState("");

  const getData = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };

  useEffect(() => {
    getData("https://shop24h-backend-namvu.herokuapp.com/products")
      .then((data) => {
        let dataList = data.data;
        function findProduct(id) {
          return id._id === params.productId;
        }
        setProduct(dataList.find((id) => findProduct(id)));
        //console.log(product);
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);

  return (
    <>
      <div className="container">
        <Header user={user} logoutGoogle={logoutGoogle} />
        <div style={{ marginTop: "80px" }}>
          <BreadCrumb breadcrumbData={breadcrumbData} />
          {/* component show thông tin sản phẩm */}
          <ProductDetailInfo product={product} user={user} />
          <br />
          {/* component show thông tin mô tả sản phẩm */}
          <ProductDetailDescription product={product} />
          <ViewAll />
          <br />
          {/* component show danh sách các sản phẩm liên quan */}
          <ProductDetailRelated />
        </div>
      </div>
      <Footer />
    </>
  );
}
