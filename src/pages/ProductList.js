import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import AllProducts from "../components/Products/AllProducts";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";

export default function ProductList({ user, logoutGoogle }) {
  const breadcrumbData = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Products",
      url: "/products",
    },
  ];

  return (
    <>
      <div className="container">
        <Header user={user} logoutGoogle={logoutGoogle} />
        <div style={{ marginTop: "80px" }}>
          <BreadCrumb breadcrumbData={breadcrumbData} />
          <AllProducts />
        </div>
      </div>
      <Footer />
    </>
  );
}
