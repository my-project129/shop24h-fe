import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import { useNavigate } from "react-router-dom";

export default function ProductCard({ element }) {
  const navigate = useNavigate();
  const onBtnLearnMoreClick = () => {
    navigate("/products/" + element._id);
  };

  return (
    <div className="mb-3">
      <Card>
        <CardContent>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {element.name}
          </Typography>
          <img src={element.imageUrl} alt="imageUrl" style={{maxHeight: "250px", maxWidth: "250px"}}></img>
          <Typography variant="body2">
            Buy price: {element.buyPrice}
            <br />
            Promotion price: {element.promotionPrice}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" onClick={onBtnLearnMoreClick}>
            Learn More / Buy
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}
