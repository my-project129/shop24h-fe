import SocialFooter from "./SocialFooter";
import InfoFooter from "./InfoFooter";

export default function Footer() {
  return (
    <>
      {/* Footer */}
      <footer className="text-center text-lg-start bg-light">
        {/* Section: Links  */}
        <section className="">
          <div className="container text-center text-md-start mt-5">
            {/* Grid row */}
            <div className="row mt-3">
              <InfoFooter />

              <SocialFooter />
            </div>
            {/* Grid row */}
          </div>
        </section>
        {/* Section: Links  */}
      </footer>
      {/* Footer */}
    </>
  );
}
