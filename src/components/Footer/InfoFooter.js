
export default function InfoFooter() {
    return (
        <>
           {/* Grid column */}
           <div className="col-md-3 mx-auto mb-4">
                                {/* Links */}
                                <h6 className="text-uppercase fw-bold mb-4">
                                    Products
                                </h6>
                                <p>
                                    <a href="#!" className="text-reset">Help Center</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Contec Us</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Product Help</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Warranty</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Order Status</a>
                                </p>
                            </div>
                            {/* Grid column */}

                            {/* Grid column */}
                            <div className="col-md-3 mx-auto mb-4">
                                {/* Links */}
                                <h6 className="text-uppercase fw-bold mb-4">
                                    Services
                                </h6>
                                <p>
                                    <a href="#!" className="text-reset">Help Center</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Contec Us</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Product Help</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Warranty</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Order Status</a>
                                </p>
                            </div>
                            {/* Grid column */}

                            {/* Grid column */}
                            <div className="col-md-3 mx-auto mb-md-0 mb-4">
                                {/* Links */}
                                <h6 className="text-uppercase fw-bold mb-4">
                                    Support
                                </h6>
                                <p>
                                    <a href="#!" className="text-reset">Help Center</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Contec Us</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Product Help</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Warranty</a>
                                </p>
                                <p>
                                    <a href="#!" className="text-reset">Order Status</a>
                                </p>
                            </div>
                            {/* Grid column */}
        </>
    )
}