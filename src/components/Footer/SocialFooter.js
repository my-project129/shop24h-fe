import Logo from "../Header/Logo";
import {
  AiFillFacebook,
  AiFillInstagram,
  AiFillYoutube,
  AiOutlineTwitter,
} from "react-icons/ai";

export default function SocialFooter() {
  return (
    <>
      {/* Grid column */}
      <div className="col-md-3 mt-3 mb-md-0 mb-4">
        <Logo /> <br />
        <AiFillFacebook /> <AiFillInstagram /> <AiFillYoutube />{" "}
        <AiOutlineTwitter />
      </div>
      {/* Grid column */}
    </>
  );
}
