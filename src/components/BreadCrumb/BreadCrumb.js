import * as React from "react";
import Breadcrumbs from "@mui/material/Breadcrumbs";
// import Link from "@mui/material/Link";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";

export default function BreadCrumb({ breadcrumbData }) {
  // let lastParamOfURL = window.location.href.substring(
  //   window.location.href.lastIndexOf("/") + 1
  // );
  // const [color, setColor] = useState("gray");
  // const [textDecoration, setTextDecoration] = useState("none");

  // const onEnterBreadCrumb = (event) => {};

  // const onLeaveBreadCrumb = (event) => {};

  return (
    <div role="presentation">
      <Breadcrumbs aria-label="breadcrumb">
        {breadcrumbData.map((element, index) => {
          return (
            // <Link
            //   key={index}
            //   underline="hover"
            //   color="inherit"
            //   href={element.url}
            // >
            //   {element.name}
            // </Link>

            <Link
              key={index}
              to={element.url}
              style={{ color: "gray", textDecoration: "none" }}
              // onMouseEnter={onEnterBreadCrumb}
              // onMouseLeave={onLeaveBreadCrumb}
            >
              {element.name}
            </Link>
          );
        })}
      </Breadcrumbs>
    </div>
  );
}
