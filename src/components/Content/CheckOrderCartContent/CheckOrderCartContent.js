// import dataList from "../../../../data.json";
import {
  Table,
  TableContainer,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography,
  Modal,
  Box,
  Input,
  FormControl,
  InputLabel,
  Snackbar,
  Alert,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Row, Col, Button } from "react-bootstrap";

export default function CheckOrderCartContent({ user }) {
  const [dataMap, setDataMap] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [allUsers, setAllUsers] = useState([]);
  const [dataUser, setDataUser] = useState([]);
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [note, setNote] = useState("");

  const [alertStatus, setAlertStatus] = useState("error");
  const [alertMessage, setAlertMessage] = useState("");
  const [openSnackbar, setOpenSnackbar] = useState(false);

  let vObjectRequest = {
    fullName: fullName,
    email: email,
    phone: phone,
    address: address,
    city: city,
    country: country,
  };

  let orderId = "";

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const handleChangeName = (event) => {
    setFullName(event.target.value);
  };
  const handleChangeEmail = (event) => {
    setEmail(event.target.value);
  };
  const handleChangePhone = (event) => {
    setPhone(event.target.value);
  };
  const handleChangeAddress = (event) => {
    setAddress(event.target.value);
  };
  const handleChangeCity = (event) => {
    setCity(event.target.value);
  };
  const handleChangeCountry = (event) => {
    setCountry(event.target.value);
  };
  const handleChangeNote = (event) => {
    setNote(event.target.value);
  };

  const validateData = (paramOrderData) => {
    if (paramOrderData.fullName === "") {
      setAlertStatus("error");
      setAlertMessage("Please fill in Full Name");
      setOpenSnackbar(true);
      return false;
    }
    if (paramOrderData.email === "") {
      setAlertStatus("error");
      setAlertMessage("Please fill in Email");
      setOpenSnackbar(true);
      return false;
    }
    if (paramOrderData.phone === "") {
      setAlertStatus("error");
      setAlertMessage("Please fill in Phone");
      setOpenSnackbar(true);
      return false;
    }
    return true;
  };

  // Phần alrert
  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar(false);
  };

  const onConfirmClick = () => {
    //console.log(vObjectRequest);
    // Kiểm tra dữ liệu
    var vIsDataValidate = validateData(vObjectRequest);
    if (vIsDataValidate) {
      handleClose(); // Đóng modal
      setAlertStatus("success");
      setAlertMessage("Thank you for your order");
      setOpenSnackbar(true); // Hiện thông báo

      // POST thêm order
      var retrievedData = sessionStorage.getItem("orderArray");
      var retrievedArray = JSON.parse(retrievedData);
      //console.log(retrievedArray);
      getData(
        `https://shop24h-backend-namvu.herokuapp.com/customers/${dataUser._id}/orders/`,
        {
          method: "POST",
          body: JSON.stringify({
            shippedDate: null,
            note: note,
            orderDetail: retrievedArray,
            cost: totalPrice,
          }),
          headers: {
            "Content-Type": "application/json;charset=UTF-8",
          },
        }
      )
        .then((data) => {
          console.log(data);
          orderId = data.data._id;
          // Cập nhật thông tin khách hàng
          getData(
            `https://shop24h-backend-namvu.herokuapp.com/customers/${dataUser._id}`,
            {
              method: "PUT",
              body: JSON.stringify({
                fullName: fullName,
                phone: phone,
                email: email,
                address: address,
                city: city,
                country: country,
                order: orderId,
              }),
              headers: {
                "Content-Type": "application/json;charset=UTF-8",
              },
            }
          )
            .then((data) => {
              //console.log(data);
            })
            .catch((error) => {
              //console.log(error);
            });
        })
        .catch((error) => {
          //console.log(error);
        });
    }
  };

  const onChangeQuantityInput = (event, index) => {
    const newDataMap = [...dataMap];
    dataMap[index].quantity = event.target.value;
    setDataMap(newDataMap);
    // Cập nhật order trong sessionStorage
    sessionStorage.setItem("orderArray", JSON.stringify(newDataMap));
    // Get total price
    var total = 0;
    for (var i in dataMap) {
      total += dataMap[i].quantity * dataMap[i].promotionPrice;
    }
    setTotalPrice(total);
  };

  const onClickDelete = (event, index) => {
    const newDataMap = [...dataMap];
    newDataMap.splice(index, 1);
    setDataMap(newDataMap);
    // Cập nhật order trong sessionStorage
    sessionStorage.setItem("orderArray", JSON.stringify(newDataMap));
  };

  const fillInModal = (paramDataUser) => {
    setFullName(paramDataUser.fullName);
    setEmail(paramDataUser.email);
    setPhone(paramDataUser.phone);
    setAddress(paramDataUser.address);
    setCity(paramDataUser.city);
    setCountry(paramDataUser.country);
  };

  const getData = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };

  useEffect(() => {
    getData("https://shop24h-backend-namvu.herokuapp.com/products/")
      .then((data) => {
        //console.log(data.data);

        var retrievedData = sessionStorage.getItem("orderArray");
        var retrievedArray = JSON.parse(retrievedData);
        //console.log(retrievedArray);
        setDataMap(retrievedArray);
        // Get total price
        var total = 0;
        for (var i in retrievedArray) {
          total +=
            retrievedArray[i].quantity * retrievedArray[i].promotionPrice;
        }
        setTotalPrice(total);

        // Điền thông tin vào modal
        getData(`https://shop24h-backend-namvu.herokuapp.com/customers/`)
          .then((data2) => {
            //console.log(data2.data);
            setDataUser(
              data2.data.find((element) => element.email === user.email)
            );
            fillInModal(
              data2.data.find((element) => element.email === user.email)
            );
          })
          .catch((error) => {
            //console.log(error);
          });
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);

  return (
    <>
      <h1 className="text-center fw-bold text-uppercase">Giỏ hàng của bạn</h1>

      <Row>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Image</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Promotion Price</TableCell>
                <TableCell>Change Amount</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {dataMap.map((element, index) => (
                <TableRow
                  key={index}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    <img
                      className="img-thumbnail"
                      src={element.productImg}
                      style={{ maxHeight: "100px", maxWidth: "100px" }}
                    ></img>
                  </TableCell>
                  <TableCell>{element.productName}</TableCell>
                  <TableCell>{element.promotionPrice}</TableCell>
                  <TableCell>
                    <input
                      type="number"
                      id="inp"
                      value={element.quantity}
                      onChange={(e) => onChangeQuantityInput(e, index)}
                    ></input>
                  </TableCell>
                  <TableCell>
                    <Button
                      variant="danger"
                      onClick={(e) => onClickDelete(e, index)}
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Row>
      <Row className="mt-3">
        <Col>
          <div className="text-center">Total Price: {totalPrice} VNĐ</div>
        </Col>
        <Col>
          <Button onClick={handleOpen}>Send Order</Button>
        </Col>
      </Row>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography style={{ fontWeight: "bold" }}>
            Customer Information
          </Typography>
          <FormControl variant="standard">
            <InputLabel htmlFor="component-simple">
              Full name <span className="text-danger">(*)</span>
            </InputLabel>
            <Input value={fullName} onChange={handleChangeName} />
          </FormControl>
          <FormControl variant="standard">
            <InputLabel htmlFor="component-simple">
              Email <span className="text-danger">(*)</span>
            </InputLabel>
            <Input value={email} onChange={handleChangeEmail} />
          </FormControl>
          <FormControl variant="standard">
            <InputLabel htmlFor="component-simple">
              Phone <span className="text-danger">(*)</span>
            </InputLabel>
            <Input value={phone} onChange={handleChangePhone} type="number" />
          </FormControl>
          <FormControl variant="standard">
            <InputLabel htmlFor="component-simple">Address</InputLabel>
            <Input value={address} onChange={handleChangeAddress} />
          </FormControl>
          <FormControl variant="standard">
            <InputLabel htmlFor="component-simple">City</InputLabel>
            <Input value={city} onChange={handleChangeCity} />
          </FormControl>
          <FormControl variant="standard">
            <InputLabel htmlFor="component-simple">Country</InputLabel>
            <Input value={country} onChange={handleChangeCountry} />
          </FormControl>
          <FormControl variant="standard">
            <InputLabel htmlFor="component-simple">Note</InputLabel>
            <Input value={note} onChange={handleChangeNote} />
          </FormControl>
          <FormControl className="mt-3">
            <Button onClick={onConfirmClick}>Confirm Send Order</Button>
          </FormControl>
        </Box>
      </Modal>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={alertStatus}
          sx={{ width: "100%" }}
        >
          {alertMessage}
        </Alert>
      </Snackbar>
    </>
  );
}
