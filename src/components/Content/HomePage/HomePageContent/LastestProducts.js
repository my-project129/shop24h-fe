// import dataList from "../../../../data.json";
import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import ProductCard from "../../../ProductCart/ProductCart";

export default function LastestProducts() {
  const getData = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  const [dataList, setDataList] = useState([]);
  useEffect(() => {
    getData(
      "https://shop24h-backend-namvu.herokuapp.com/products?sort=-1&limit=4"
    )
      .then((data) => {
        //console.log(data);
        setDataList(data.data);
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);

  return (
    <>
      <h1 className="text-center fw-bold text-uppercase">Lastest Products</h1>
      <Row>
        {dataList.map((element, index) => {
          return (
            <Col lg={3} md={4} sm={6} key={index}>
              <ProductCard element={element} />
            </Col>
          );
        })}
      </Row>
    </>
  );
}
