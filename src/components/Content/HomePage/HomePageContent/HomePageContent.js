import CarouselComponent from "./CarouselComponent";
import LastestProducts from "./LastestProducts";
import ViewAll from "./ViewAll";

export default function HomePageContent() {
    return (
        <>
            <CarouselComponent/>
            <div className="mt-5">
                <LastestProducts/>
            </div>
            <ViewAll/>
        </>
    )
}