import { Carousel } from "react-bootstrap";

import ImgSlide1 from "../../../../assets/images/Untitled-1.jpg";
import ImgSlide2 from "../../../../assets/images/Untitled-2.jpg";
import ImgSlide3 from "../../../../assets/images/Untitled-3.jpg";

export default function CarouselComponent() {
  return (
    <div style={{ marginTop: "80px" }}>
      <Carousel>
        <Carousel.Item>
          <img className="d-block w-100" src={ImgSlide1} alt="First slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={ImgSlide2} alt="Second slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={ImgSlide3} alt="Third slide" />
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
