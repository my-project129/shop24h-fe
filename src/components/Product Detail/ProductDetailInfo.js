import { Col, Row } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

export default function ProductDetailInfo({ user }) {
  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [productInfo, setProductInfo] = useState("");
  const [typeName, setTypeName] = useState("");
  const [inpAmount, setInpAmount] = useState("");
  const [textButton, setTextButton] = useState("");

  // const { productQuantity } = useSelector((reduxData) => reduxData.taskReducer);

  const getData = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };

  useEffect(() => {
    getData(
      "https://shop24h-backend-namvu.herokuapp.com/products/" + params.productId
    )
      .then((data) => {
        ////console.log(data);
        setProductInfo(data.data);
        getData(
          "https://shop24h-backend-namvu.herokuapp.com/producttypes/" +
            data.data.type
        )
          .then((data) => {
            ////console.log(data);
            setTypeName(data.data.name);
          })
          .catch((error) => {
            //console.log(error);
          });
        // Set text button
        if (user === null || user === undefined) {
          setTextButton("Login to buy");
        } else {
          setTextButton("Add to cart");
        }
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);

  const onProductAmountChangeHandler = (event) => {
    setInpAmount(event.target.value);
  };

  const onAddToCartClickHandler = () => {
    // ordersObject = {
    //   ...ordersObject,
    //   orderArray: [...ordersObject.orderArray, newOrderObject],
    // };
    if (user === null || user === undefined) {
      //console.log("chưa đăng nhập");
      navigate("/login");
    } else {
      const newOrderObject = {
        productId: params.productId,
        quantity: inpAmount,
        productImg: productInfo.imageUrl,
        productName: productInfo.name,
        productType: typeName,
        promotionPrice: productInfo.promotionPrice,
      };

      var retrievedData = sessionStorage.getItem("orderArray");

      if (retrievedData === undefined || retrievedData === null) {
        let newArray = [];
        newArray.push(newOrderObject);
        sessionStorage.setItem("orderArray", JSON.stringify(newArray));
        dispatch({
          type: "ADD_TO_CART",
        });
      } else {
        var retrievedArray = JSON.parse(retrievedData);
        let flag = false;
        for (let bI = 0; bI < retrievedArray.length; bI++) {
          if (retrievedArray[bI].productId === newOrderObject.productId) {
            flag = true;
            retrievedArray[bI].quantity = newOrderObject.quantity;
            sessionStorage.setItem(
              "orderArray",
              JSON.stringify(retrievedArray)
            );
          }
        }
        if (!flag) {
          retrievedArray.push(newOrderObject);
          sessionStorage.setItem("orderArray", JSON.stringify(retrievedArray));
          dispatch({
            type: "ADD_TO_CART",
          });
        }
      }
    }
  };

  const onBtnGoBackClickHandler = () => {
    navigate("/products");
  };
  return (
    <div>
      <Row>
        <Col lg={5} md={5} sm={12}>
          <Row>
            <div
              style={{
                maxWidth: "250px",
                margin: "auto",
                width: "50%",
                paddingTop: "20px",
              }}
            >
              <img
                src={productInfo.imageUrl}
                style={{ display: "block", width: "100%", marginRight: "0px" }}
                alt="productInfo.imageUrl"
              ></img>
            </div>
          </Row>
        </Col>
        <Col lg={7} md={7} sm={12} className="p-5">
          <Row>Tên sản phẩm: {productInfo.name}</Row>
          <Row>Type: {typeName}</Row>
          <Row>Promotion price: {productInfo.promotionPrice}</Row>
          <Row>Số lượng muốn mua: </Row>
          <Row>
            <input
              className="form-control"
              style={{ width: "250px" }}
              type="number"
              min="0"
              placeholder="Nhập số lượng"
              inputProps={{ "aria-label": "description" }}
              value={inpAmount}
              onChange={onProductAmountChangeHandler}
            />
          </Row>
          <Row>
            <Col lg={6} sm={6} className="mt-3">
              <button
                className="btn btn-success m-2"
                onClick={onAddToCartClickHandler}
              >
                {textButton}
              </button>
              <button
                className="btn btn-primary m-2"
                onClick={onBtnGoBackClickHandler}
              >
                Go Back
              </button>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
