// import dataList from "../../data.json"
import { Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import ProductCard from "../ProductCart/ProductCart";

export default function ProductDetailRelated() {
  const limit = 2;

  const getData = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  const [dataList, setDataList] = useState([]);
  useEffect(() => {
    getData("https://shop24h-backend-namvu.herokuapp.com/products")
      .then((data) => {
        setDataList(data.data.slice(0, limit));
        //console.log(data.data);
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);
  return (
    <>
      <h5>Related Products</h5>
      <Row>
        {dataList.map((element, index) => {
          return (
            <Col xs={3} key={index}>
              <ProductCard element={element} />
            </Col>
          );
        })}
      </Row>
    </>
  );
}
