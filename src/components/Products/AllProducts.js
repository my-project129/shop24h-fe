// import dataList from "../../data.json"
import { Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import ProductCard from "../ProductCart/ProductCart";
import {
  Pagination,
  Button,
  Input,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
} from "@mui/material";

export default function AllProducts() {
  const limit = 8;
  const [dataList, setDataList] = useState([]);
  const [productTypesList, setProductTypesList] = useState([]);
  const [page, setPage] = useState(1);
  const [type, setType] = useState("");
  const [noPage, setNoPage] = useState(10);
  const [name, setName] = useState("");
  const [minPromotionPrice, setMinPromotionPrice] = useState("");
  const [maxPromotionPrice, setMaxPromotionPrice] = useState("");
  const [buttonClicked, setButtonClicked] = useState(0);

  const onButtonFilterClicked = () => {
    //console.log(buttonClicked);
    setButtonClicked(buttonClicked + 1);
  };

  const onInputProductNameChangeHandler = (event) => {
    setName(event.target.value);
  };

  const onInputMinPromotionPriceChangeHandler = (event) => {
    setMinPromotionPrice(event.target.value);
  };

  const onInputMaxPromotionPriceChangeHandler = (event) => {
    setMaxPromotionPrice(event.target.value);
  };

  const onSelectTypeChange = (event) => {
    setType(event.target.value);
  };

  const getData = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };

  const changePageHandle = (event, value) => {
    setPage(value);
  };

  const condition =
    "&name=" +
    name +
    "&type=" +
    type +
    "&minPromotionPrice=" +
    minPromotionPrice +
    "&maxPromotionPrice=" +
    maxPromotionPrice +
    "&page=" +
    page +
    "&limit=" +
    limit;

  useEffect(() => {
    getData("https://shop24h-backend-namvu.herokuapp.com/products")
      .then((data) => {
        setNoPage(Math.ceil(data.data.length / limit));
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);

  useEffect(() => {
    getData("https://shop24h-backend-namvu.herokuapp.com/producttypes")
      .then((data) => {
        setProductTypesList(data.data);
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);

  useEffect(() => {
    getData("https://shop24h-backend-namvu.herokuapp.com/products?" + condition)
      .then((data) => {
        //console.log(data);

        setDataList(data.data);
      })
      .catch((error) => {
        //console.log(error);
      });
  }, [page, buttonClicked]);
  return (
    <>
      <h1 className="text-center fw-bold text-uppercase">All Products</h1>
      <Row>
        <Col md={3} sm={1}>
          <Input
            placeholder="Nhập tên sản phẩm"
            inputProps={{ "aria-label": "description" }}
            value={name}
            onChange={onInputProductNameChangeHandler}
          />
        </Col>
        <Col md={3} sm={1}>
          <Input
            type="number"
            placeholder="Nhập giá thấp nhấp"
            inputProps={{ "aria-label": "description" }}
            value={minPromotionPrice}
            onChange={onInputMinPromotionPriceChangeHandler}
          />
        </Col>
        <Col md={3} sm={1}>
          <Input
            type="number"
            placeholder="Nhập giá cao nhấp"
            inputProps={{ "aria-label": "description" }}
            value={maxPromotionPrice}
            onChange={onInputMaxPromotionPriceChangeHandler}
          />
        </Col>
        <Col md={3} sm={1}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Chọn type</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={type}
              label="Chọn type"
              onChange={onSelectTypeChange}
            >
              <MenuItem value="Tất cả">Tất cả</MenuItem>
              {productTypesList.map((element, index) => {
                return (
                  <MenuItem key={index} value={element._id}>
                    {element.name}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <div className="d-flex justify-content-center">
          <Button
            variant="contained"
            color="success"
            onClick={onButtonFilterClicked}
            style={{ width: "200px" }}
          >
            Lọc
          </Button>
        </div>
      </Row>
      <Row>
        {dataList.map((element, index) => {
          return (
            <Col lg={3} md={4} sm={6} key={index}>
              <ProductCard element={element} />
            </Col>
          );
        })}
      </Row>
      <Row>
        <div className="d-flex mt-3 justify-content-end">
          <Pagination
            count={noPage}
            defaultPage={page}
            color="secondary"
            onChange={changePageHandle}
          ></Pagination>
        </div>
      </Row>
    </>
  );
}
