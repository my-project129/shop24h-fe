// const intialiState = {
//   orders: [],
// };
// const OrderCart = (state = intialiState, action) => {
//   switch (action.type) {
//     case "ADD_TO_CART": {
//       let flag = false;
//       for (let bI = 0; bI < state.orders.length; bI++) {
//         if (state.orders[bI].productId === action.payload.orderNew.productId) {
//           state.orders[bI].productQuantity =
//             state.orders[bI].productQuantity +
//             action.payload.orderNew.productQuantity;
//           flag = true;
//         }
//       }
//       if (!flag) {
//         state.orderNew = [...state.orders, action.payload.orderNew];
//       }
//       return {
//         ...state,
//       };
//     }
//     default: {
//       return state;
//     }
//   }
// };

// export default OrderCart;

const intialiState = {
  productQuantity: 0,
};
const OrderCart = (state = intialiState, action) => {
  switch (action.type) {
    case "ADD_TO_CART": {
      return {
        ...state,
        productQuantity: state.productQuantity + 1,
      };
    }
    default: {
      return state;
    }
  }
};

export default OrderCart;
