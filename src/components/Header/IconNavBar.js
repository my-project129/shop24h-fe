import { IoIosNotificationsOutline } from "react-icons/io";
import { BiUserCircle } from "react-icons/bi";
import { FiShoppingCart } from "react-icons/fi";

import { Badge } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";

export default function IconNavbar() {
  const { productQuantity } = useSelector((reduxData) => reduxData.taskReducer);

  useEffect(() => {}, [productQuantity]);

  const navigate = useNavigate();
  const onIconShoppingClick = () => {
    navigate("/checkOrderCart");
  };
  return (
    <>
      <IoIosNotificationsOutline size={20} className="me-2" />
      <BiUserCircle size={20} className="me-2" />
      <Badge badgeContent={productQuantity} color="primary">
        <FiShoppingCart
          size={20}
          style={{ cursor: "pointer" }}
          onClick={onIconShoppingClick}
        />
      </Badge>
    </>
  );
}
