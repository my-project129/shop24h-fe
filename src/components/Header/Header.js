import LoginImage from "./Header/LoginImage";

import Logo from "./Logo";
import IconNavbar from "./IconNavBar";
import { useNavigate } from "react-router-dom";

export default function Header({ user, logoutGoogle }) {
  const navigate = useNavigate();

  const onBtnLoginClick = () => {
    navigate("/login");
  };
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">
            <Logo />
          </a>

          <div
            className="collapse navbar-collapse"
            id="navbarSupportedContent"
          ></div>

          {user ? (
            <>
              <IconNavbar className="me-3" />
              <LoginImage user={user} logoutGoogle={logoutGoogle} />
              {/* <img src={user.photoURL} width={50} height={50} alt="avatar" style={{ borderRadius: "50%" }}></img>
                            <p>Hello, {user.displayName}</p> */}
            </>
          ) : (
            <button className="btn btn-danger" onClick={onBtnLoginClick}>
              Login
            </button>
          )}
        </div>
      </nav>
    </>
  );
}
