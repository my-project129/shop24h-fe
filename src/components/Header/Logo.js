import LogoImage from "../../assets/images/logo-full.png";

export default function Logo() {
  return (
    <>
      <img style={{ maxHeight: "50px" }} src={LogoImage} alt="LogoImage"></img>
    </>
  );
}
