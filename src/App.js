import Login from "./pages/Login";
import HomePage from "./pages/HomePage";
import ProductList from "./pages/ProductList";
import ProductInfo from "./pages/ProductInfo";
import CheckOrderCart from "./pages/CheckOrderCart";

import { Route, Routes } from "react-router-dom";
import { auth, googleProvider } from "./firebase";
import { useState, useEffect } from "react";

function App() {
  const [user, setUser] = useState(null);

  const loginGoogle = () => {
    auth
      .signInWithPopup(googleProvider)
      .then((result) => {
        //console.log(result);
        setUser(result.user);
      })
      .catch((error) => {
        //console.log(error);
      });
  };

  const logoutGoogle = () => {
    auth
      .signOut()
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        //console.log(error);
      });
  };

  useEffect(() => {
    auth.onAuthStateChanged((result) => {
      //console.log(result);
      setUser(result);
    });
  }, []);

  return (
    <div>
      <Routes>
        <Route
          exact
          path="/"
          element={<HomePage user={user} logoutGoogle={logoutGoogle} />}
        ></Route>

        <Route
          path="/products"
          element={<ProductList user={user} logoutGoogle={logoutGoogle} />}
        ></Route>

        <Route
          path="/login"
          element={<Login loginGoogle={loginGoogle} />}
        ></Route>

        <Route
          path="products/:productId"
          element={<ProductInfo user={user} logoutGoogle={logoutGoogle} />}
        ></Route>

        <Route
          path="/checkOrderCart"
          element={<CheckOrderCart user={user} logoutGoogle={logoutGoogle} />}
        ></Route>
      </Routes>
    </div>
  );
}

export default App;
